class PreOrderApplet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentStage: 0,
            meal: "",
            numPeople: 0,
            restaurant: "",
            dishes: [{name: "", numOrders: 1}],
            maxInputs: 10
        };
        this.changeStage = this.changeStage.bind(this);
        this.updateModel = this.updateModel.bind(this);
        this.checkNums = this.checkNums.bind(this);
        this.checkNames = this.checkNames.bind(this);
        this.submit = this.submit.bind(this);
        this.addDish = this.addDish.bind(this);
        this.deleteDish = this.deleteDish.bind(this);
    };

    submit(){
        console.log({
            meal: this.state.meal,
            numPeople: this.state.numPeople,
            restaurant: this.state.restaurant,
            dishes: this.state.dishes
        })
    }

    totalDishes() {
        var total = 0;

        for (var dish in this.state.dishes){
            if (this.state.dishes[dish]){
                total += this.state.dishes[dish].numOrders;
            }
        }

        return total;
    };

    addDish(){
        var buf = this.state.dishes;
        buf.push({name: "", numOrders: 1})
        console.log(this.state.dishes)
        this.setState({
            dishes: buf
        });
    }

    deleteDish(){
        var buf = this.state.dishes;
        buf.pop()
        console.log(this.state.dishes)
        this.setState({
            dishes: buf
        });
    }

    // checkValidDishName(name){
    //     var currDishNames = [];
    //     for (var i in this.state.dishes){
    //         currDishNames.push(this.state.dishes[i].name);
    //     }

    //     if (!currDishNames.includes(name)){
    //         return true;
    //     }
    //     return false;
    // }

    checkNames(){
        var name = true;
        var nameBuf = [];

        for (var i in this.state.dishes){
            if (nameBuf.includes(this.state.dishes[i].name)){
                name = false;
            }
            if (!this.state.dishes[i].name){
                name = false;
            }
            nameBuf.push(this.state.dishes[i].name);
        }


        return name;
    }

    checkNums(){
        var count = true;
        var total = 0;

        for (var i in this.state.dishes){
            if (this.state.dishes[i].numOrders <= 0 || 
                this.state.dishes[i].numOrders > this.state.maxInputs){

                count = false;
            }
            total = total + parseInt(this.state.dishes[i].numOrders);
        }

        if (total > this.state.maxInputs || total < this.state.numPeople){
            count = false
        }

        return count;
    }

    checkHighestStage() {
        if (!this.state.meal || !(this.state.numPeople > 0 && this.state.numPeople <= this.state.maxInputs)){
            return 0;
        }
        else if (!this.state.restaurant){
            return 1;
        }   
        else if (!(this.checkNames() && this.checkNums())){
            return 2;
        }
        else{
            return 3;
        }
    };

    changeStage(i) {
        this.setState({'currentStage': i});
    };

    updateModel(newModel) {
        this.setState(newModel);
    };

    render() {
        return(
            <div className="pre-order-applet container">
                <div className="top-nav-bar container-fluid">
                    <TopNavBar
                        maxStage={this.checkHighestStage()}
                        currentStage={this.state.currentStage}
                        changeStage={this.changeStage}
                    />
                </div>
                <div className="form-body">
                    <FormBody
                        currentStage={this.state.currentStage}
                        updateModel={this.updateModel}
                        maxInputs={this.state.maxInputs}
                        numPeople={this.state.numPeople}
                        meal={this.state.meal}
                        restaurant={this.state.restaurant}
                        dishes={this.state.dishes}
                        checkNums={this.checkNums}
                        checkNames={this.checkNames}
                        addDish={this.addDish}
                        deleteDish={this.deleteDish}
                    />
                </div>
                <div className="bot-nav-bar container-fluid">
                    <BotNavBar
                        maxStage={this.checkHighestStage()}
                        currentStage={this.state.currentStage}
                        changeStage={this.changeStage}
                        submit={this.submit}
                    />            
                </div>
            </div>
        );
    };
};

function TopNavButton(props) {
    var classes = "btn col-md-2 " + (props.isSelected ? "btn-secondary" : "btn-outline-secondary");
    return(
        <button type="button" className={classes}
                disabled={props.isAvailable}
                onClick={props.onClick}>
            {props.value}
        </button>
    );
};

class TopNavBar extends React.Component {
    renderButton(i) {
        return(
            <TopNavButton
                onClick={() => this.props.changeStage(i)}
                isSelected={this.props.currentStage == i}
                isAvailable={this.props.maxStage < i}
                value={i==3 ? "Review" : "Stage " + (i + 1)}
            />
        );
    };

    render() {
        return(
            <div class="row">
                {this.renderButton(0)}
                {this.renderButton(1)}
                {this.renderButton(2)}
                {this.renderButton(3)}
            </div>
        );
    };
};

class BotNavBar extends React.Component {
    render() {
        var currentStage = this.props.currentStage;
        var prevButtonClass = "btn btn-outline-primary col-sm-2 " + (currentStage == 0 ? "hidden" : "");
        return(
            <div class="row">
                <button type="button" className={prevButtonClass}
                        onClick={() => this.props.changeStage(currentStage-1)}>
                    Previous
                </button>
                { 
                    currentStage == 3
                    ? <button type="button" class="btn btn-info offset-sm-8 col-sm-2"
                        onClick={() => this.props.submit()}>
                        Submit
                      </button> 
                    : <button type="button" class="btn btn-outline-primary offset-sm-8 col-sm-2"
                        onClick={() => this.props.changeStage(currentStage+1)}
                        disabled={this.props.maxStage < (currentStage+1)}>
                        Next
                      </button>
                }
            </div>
        );
    };
};

function MealSelector(props) {
    var classes = "centre " + (props.hasError ? "error":"");
    return(
        <select className={classes} 
                id="meal"
                value={props.meal}
                onChange={props.getMealVal}>
            <option value="">---</option>
            <option value="breakfast">Breakfast</option>
            <option value="lunch">Lunch</option>
            <option value="dinner">Dinner</option>
        </select>
    );
};

function NumPeopleInput(props) {
    var classes = "centre small-num-in " + (props.hasError ? "error":"");
    return(
        <input className={classes}
               type="number"
               value={props.numPeople}
               id="numPeople"
               onChange={props.handleNumChange}/>
    );
}

class Stage1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initNumPeople: true,
            initMeal: true
        }
        this.getMealVal = this.getMealVal.bind(this);
        this.handleNumChange = this.handleNumChange.bind(this);
        //this.isMealValid = this.isMealValid.bind(this);
        //this.isNumPeopleValid = this.isNumPeopleValid.bind(this);
    };
    
    isNumPeopleValid(){
        if (this.state.initNumPeople) { return true; }
        if (this.props.numPeople < 1 || this.props.numPeople > this.props.maxInputs){
            return false;
        }
        return true;
    }

    handleNumChange(event) {
        this.setState({initNumPeople: false})
        this.props.updateModel({numPeople: event.target.value});
    };

    isMealValid(){
        if (this.state.initMeal) { return true; }
        if (!this.props.meal){
            return false;
        }
        return true;
    }

    getMealVal(event) {
        this.setState({initMeal: false})
        this.props.updateModel({meal: event.target.value});
    };

    render() {
        return(
            <div class={"container-fluid " + (this.props.isHidden ? "swappedOut" : "")}>
                <div class="row">
                    <div class="field-label centre">Please Select a Meal</div>
                </div>
                <div class="row">
                    <MealSelector getMealVal={this.getMealVal}
                                  meal={this.props.meal}
                                  hasError={!this.isMealValid()}/>
                </div>
                <div class="row">
                    <small className={"text-danger centre " + (this.isMealValid() ? "hidden" : "")}>
                        Please Select a Meal
                    </small>
                </div>
                <div class="row">
                    <div class="field-label centre">Please Enter Number of People</div>
                </div>
                <div class="row">
                    <NumPeopleInput handleNumChange={this.handleNumChange}
                                    numPeople={this.props.numPeople}
                                    maxInputs={this.props.maxInputs}
                                    hasError={!this.isNumPeopleValid()}/>                    
                </div>
                <div class="row">
                    <small className={"text-danger centre " + (this.isNumPeopleValid() ? "hidden" : "")}>
                        Please enter a number from 1 - {this.props.maxInputs}
                    </small>
                </div>
            </div>
        );
    };
};

function RestaurantSelector(props) {
    var classes = "centre " + (props.hasError ? "error":"");
    var availableOptions = props.restaurantDict;
    var optionTags = [];
    optionTags.push(<option value="">---</option>)
    for (var item in availableOptions){
        optionTags.push(<option value={availableOptions[item]}>{availableOptions[item]}</option>);
    }
    return(
        <select class="centre"
                className={classes} 
                id="restaurant"
                value={props.restaurant}
                onChange={props.getRestaurantVal}>
            {optionTags}
        </select>
    );
};

class Stage2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initRestaurant: true,
        }
        this.getRestaurantVal = this.getRestaurantVal.bind(this);
    };
    isRestaurantValid(){
        if (this.state.initRestaurant) { return true; }
        if (!this.props.restaurant){
            return false;
        }
        return true;
    };
    updateRestaurantDict(){
        return getRestaurantDict(this.props.meal)
    }
    getRestaurantVal(event) {
        this.setState({initRestaurant: false})
        this.props.updateModel({restaurant: event.target.value});
    };
    render() {
        return(
            <div class={"container-fluid " + (this.props.isHidden ? "swappedOut" : "")}>
                <div class="row">
                    <div class="field-label centre">Please Select a Restaurant</div>
                </div>
                <div class="row">
                    <RestaurantSelector getRestaurantVal={this.getRestaurantVal}
                                        restaurantDict={this.updateRestaurantDict()}
                                        restaurant={this.props.restaurant}
                                        hasError={!this.isRestaurantValid()}/>
                </div>
                <div class="row">
                    <small className={"text-danger centre " + (this.isRestaurantValid() ? "hidden" : "")}>
                        Please Select a Restaurant
                    </small>
                </div>
            </div>
        );
    };
};

class DishRow extends React.Component {
    render(){
        var availableOptions = this.props.dishDict;
        var optionTags = [];
        optionTags.push(<option value="">---</option>)
        for (var item in availableOptions){
            optionTags.push(<option value={availableOptions[item]}>{availableOptions[item]}</option>);
        }
        return(
            <div class="row">
                <div class="field-label col-md-6 order-md-1">Please Select a Dish</div>
                <div class="field-wrapper col-md-6 order-md-3">                       
                    <select className={"field " + (this.props.hasNameError ? "error" : "")} 
                            id="dish"
                            value={this.props.dish.name}
                            onChange={(e)=>this.props.getDishVal(this.props.index, e)}>
                        {optionTags}
                    </select>
                </div>
                <div class="field-label col-md-6 order-md-2">Please Enter no. of Servings</div>
                <div class="field-wrapper col-md-6 order-md-4">
                    <input className={"small-num-in "  + (this.props.hasNumError ? "error" : "")} 
                           value={this.props.dish.numOrders} 
                           type="number"
                           onChange={(e)=>this.props.getDishCount(this.props.index, e)}/>
                </div>
            </div>
        );
    }
}

class Stage3 extends React.Component {
    constructor(props) {
        super(props);
        this.getDishCount = this.getDishCount.bind(this);
        this.getDishVal = this.getDishVal.bind(this);
        // this.updateDishDict = this.updateDishDict.bind(this)
    };
    getDishCount(index, event) {
        const buf = this.props.dishes;
        buf[index].numOrders = event.target.value;
        this.props.updateModel({
            dishes: buf
        });
    }
    getDishVal(index, event) {
        const buf = this.props.dishes;
        buf[index].name = event.target.value;
        this.props.updateModel({
            dishes: buf
        });
    }
    updateDishDict() {
        var arr = getDishDict(this.props.restaurant, this.props.meal)
        return arr;
    }
    canAdd() {
        var arr = getDishDict(this.props.restaurant, this.props.meal)
        return (arr.length > this.props.dishes.length);
    }
    render() {
        var jsxDishRows = [];
        for (var i in this.props.dishes){
            jsxDishRows.push(
                <DishRow
                    getDishCount={this.getDishCount}
                    getDishVal={this.getDishVal}
                    dishDict={this.updateDishDict()}
                    dish={this.props.dishes[i]}
                    hasNumError={!this.props.checkNums()}
                    hasNameError={!this.props.checkNames()}
                    index={i}
                />
            );
        }
        return(
            <div class={"container-fluid " + (this.props.isHidden ? "swappedOut" : "")}>
                {jsxDishRows}
                <div class="row">
                    <div class="field-wrapper col-sm-1">
                        <button type="button" 
                                className={"btn btn-outline-dark " + (this.canAdd() ? "" : "hidden")}
                                onClick={this.props.addDish}>
                            +
                        </button>
                    </div>
                    <div class="field-wrapper col-sm-1">
                        <button type="button" 
                                className={"btn btn-outline-dark " + (this.props.dishes.length == 1 ? "hidden" : "")}
                                onClick={this.props.deleteDish}>
                            -
                        </button>
                    </div>
                </div>
                <div class="row">
                    <small className={"text-danger centre " + (this.props.checkNums() ? "hidden" : "")}>
                        Please ensure you have the made the required orders for your group. <br/> Maximum {this.props.maxInputs} orders.
                    </small>
                </div>
                <div class="row">
                    <small className={"text-danger centre " + (this.props.checkNames() ? "hidden" : "")}>
                        Please ensure you do not repeat orders or leave them blank.
                    </small>
                </div>
            </div>
        );
    };
};

class Stage4 extends React.Component {
    render() {
        var dishArr = this.props.dishes;
        var jsxDishes = [];
        for (var item in dishArr){
            jsxDishes.push(<li class="list-group-item list-group-item-light justify-content-between d-flex">
                                {dishArr[item].name}
                                <span class="badge badge-secondary badge-pill">x{dishArr[item].numOrders}</span>
                           </li>);
        }
        return(
            <div class={"container-fluid " + (this.props.isHidden ? "swappedOut" : "")}>
                <div class="row">
                    <div class="field-label col-sm-5">Meal</div>
                    <div class="col-sm-7">{this.props.meal.charAt(0).toUpperCase()+this.props.meal.substr(1)}</div>
                </div>
                <div class="row">
                    <div class="field-label col-sm-5">No. of People</div>
                    <div class="col-sm-7">{this.props.numPeople}</div>
                </div>
                <div class="row">
                    <div class="field-label col-sm-5">Restaurant</div>
                    <div class="col-sm-7">{this.props.restaurant}</div>
                </div>
                <div class="row">
                    <div class="field-label col-sm-5">Dishes</div>
                    <ul class="col-sm-7 list-group">
                        {jsxDishes}
                    </ul>
                </div>
            </div>
        );
    };
};

class FormBody extends React.Component {
    render() {
        return(
            <div>
                <Stage1 isHidden={this.props.currentStage != 0}
                        updateModel={this.props.updateModel}
                        maxInputs={this.props.maxInputs}
                        numPeople={this.props.numPeople}
                        meal={this.props.meal}/>
                <Stage2 isHidden={this.props.currentStage != 1}
                        updateModel={this.props.updateModel}
                        meal={this.props.meal}
                        restaurant={this.props.restaurant}/>
                <Stage3 isHidden={this.props.currentStage != 2}
                        restaurant={this.props.restaurant}
                        meal={this.props.meal}
                        updateModel={this.props.updateModel}
                        numPeople={this.props.numPeople}
                        maxInputs={this.props.maxInputs}
                        dishes={this.props.dishes}
                        checkNames={this.props.checkNames}
                        checkNums={this.props.checkNums}
                        addDish={this.props.addDish}
                        deleteDish={this.props.deleteDish}
                        checkValidDishName={this.props.checkValidDishName}/>
                <Stage4 isHidden={this.props.currentStage != 3}
                        meal={this.props.meal}
                        numPeople={this.props.numPeople}
                        restaurant={this.props.restaurant}
                        dishes={this.props.dishes}/>
            </div>
        );
    };
};

ReactDOM.render(<PreOrderApplet/>, document.getElementById("root"));
