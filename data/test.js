var dishes = JSON.parse('[{"id":1,"name":"ChickenBurger","restaurant":"McDonalds","availableMeals":["lunch","dinner"]},{"id":2,"name":"HamBurger","restaurant":"McDonalds","availableMeals":["lunch","dinner"]},{"id":3,"name":"CheeseBurger","restaurant":"McDonalds","availableMeals":["lunch","dinner"]},{"id":4,"name":"Fries","restaurant":"McDonalds","availableMeals":["lunch","dinner"]},{"id":5,"name":"EggMuffin","restaurant":"McDonalds","availableMeals":["breakfast"]},{"id":6,"name":"Burrito","restaurant":"TacoBell","availableMeals":["lunch","dinner"]},{"id":7,"name":"Tacos","restaurant":"TacoBell","availableMeals":["lunch","dinner"]},{"id":8,"name":"Quesadilla","restaurant":"TacoBell","availableMeals":["lunch","dinner"]},{"id":9,"name":"Steak","restaurant":"BBQHut","availableMeals":["dinner"]},{"id":10,"name":"Yakitori","restaurant":"BBQHut","availableMeals":["dinner"]},{"id":11,"name":"Nankotsu","restaurant":"BBQHut","availableMeals":["dinner"]},{"id":12,"name":"Piman","restaurant":"BBQHut","availableMeals":["dinner"]},{"id":13,"name":"VeganBento","restaurant":"VegeDeli","availableMeals":["lunch"]},{"id":14,"name":"ColeslawSandwich","restaurant":"VegeDeli","availableMeals":["breakfast"]},{"id":15,"name":"GrilledSandwich","restaurant":"VegeDeli","availableMeals":["breakfast"]},{"id":16,"name":"Veg.Salad","restaurant":"VegeDeli","availableMeals":["lunch","dinner"]},{"id":17,"name":"FruitSalad","restaurant":"VegeDeli","availableMeals":["lunch","dinner"]},{"id":18,"name":"CornSoup","restaurant":"VegeDeli","availableMeals":["lunch","dinner"]},{"id":19,"name":"TomatoSoup","restaurant":"VegeDeli","availableMeals":["lunch","dinner"]},{"id":20,"name":"MinestroneSoup","restaurant":"VegeDeli","availableMeals":["lunch","dinner"]},{"id":21,"name":"PepperoniPizza","restaurant":"Pizzeria","availableMeals":["lunch","dinner"]},{"id":22,"name":"PepperoniPizza","restaurant":"Pizzeria","availableMeals":["lunch","dinner"]},{"id":23,"name":"HawaiianPizza","restaurant":"Pizzeria","availableMeals":["lunch","dinner"]},{"id":24,"name":"SeafoodPizza","restaurant":"Pizzeria","availableMeals":["lunch","dinner"]},{"id":25,"name":"DeepDishPizza","restaurant":"Pizzeria","availableMeals":["dinner"]},{"id":26,"name":"ChowMein","restaurant":"PandaExpress","availableMeals":["lunch","dinner"]},{"id":27,"name":"MapoTofu","restaurant":"PandaExpress","availableMeals":["lunch","dinner"]},{"id":28,"name":"KungPao","restaurant":"PandaExpress","availableMeals":["lunch","dinner"]},{"id":29,"name":"Wontons","restaurant":"PandaExpress","availableMeals":["lunch","dinner"]},{"id":30,"name":"GarlicBread","restaurant":"OliveGarden","availableMeals":["breakfast","lunch","dinner"]},{"id":31,"name":"Ravioli","restaurant":"OliveGarden","availableMeals":["lunch","dinner"]},{"id":32,"name":"RigatoniSpaghetti","restaurant":"OliveGarden","availableMeals":["lunch","dinner"]},{"id":33,"name":"FettucinePasta","restaurant":"OliveGarden","availableMeals":["lunch","dinner"]}]');
breakfastDishes = []
lunchDishes = []
dinnerDishes = []
for (var item in dishes){
    if (dishes[item].availableMeals.includes("breakfast")){
        breakfastDishes.push({name: dishes[item].name, restaurant: dishes[item].restaurant});
    }
    if (dishes[item].availableMeals.includes("lunch")){
        lunchDishes.push({name: dishes[item].name, restaurant: dishes[item].restaurant});
    }
    if (dishes[item].availableMeals.includes("dinner")){
        dinnerDishes.push({name: dishes[item].name, restaurant: dishes[item].restaurant});
    }
}

var mealDict = {
    "breakfast": breakfastDishes,
    "lunch": lunchDishes,
    "dinner": dinnerDishes  
}

var getRestaurantDict = function (key) {
    var restaurants = [];
    var meals = mealDict[key];
    for ( var item in meals){
        var buf = meals[item].restaurant;
        if (!restaurants.includes(buf)){
            restaurants.push(buf)
        }
    }
    return restaurants;
}

//perhaps a smarter algorithm could be accomplished in production, to cache some 

var getDishDict = function (restaurant, key){
    var dishes = [];
    var meals = mealDict[key];

    for ( var item in meals){
        var buf = meals[item].restaurant;
        var buf2 = meals[item].name;
        if (!dishes.includes(buf2) && restaurant == buf){
            dishes.push(buf2)
        }
    }

    return dishes;
}

