import dishlist from '../assets/dishes.json'

/*
	In a database scenario, various caching may be utilized.
	This implementation may be a little specific based on meals, 
	but generally functions okay as a proxy-database for this demo

	A singleton was chosen to elimate a little bit of memory footprint.
*/
class DishData {
	constructor() {
		this.dishes = dishlist.dishes;
		var breakfastDishes = []
		var lunchDishes = []
		var dinnerDishes = []
		for (var item in this.dishes){
		    if (this.dishes[item].availableMeals.includes("breakfast")){
		        breakfastDishes.push({name: this.dishes[item].name, restaurant: this.dishes[item].restaurant});
		    }
		    if (this.dishes[item].availableMeals.includes("lunch")){
		        lunchDishes.push({name: this.dishes[item].name, restaurant: this.dishes[item].restaurant});
		    }
		    if (this.dishes[item].availableMeals.includes("dinner")){
		        dinnerDishes.push({name: this.dishes[item].name, restaurant: this.dishes[item].restaurant});
		    }
		}
		this.mealDict = {
		    "breakfast": breakfastDishes,
		    "lunch": lunchDishes,
		    "dinner": dinnerDishes  
		}
	}
	getRestaurantDict(key) {
	    var restaurants = [];
	    var meals = this.mealDict[key];
	    for ( var item in meals){
	        var buf = meals[item].restaurant;
	        if (!restaurants.includes(buf)){
	            restaurants.push(buf)
	        }
	    }
	    return restaurants;
	}
	
	getDishDict(restaurant, key) {
	    var dishes = [];
	    var meals = this.mealDict[key];

	    for ( var item in meals){
	        var buf = meals[item].restaurant;
	        var buf2 = meals[item].name;
	        if (!dishes.includes(buf2) && restaurant === buf){
	            dishes.push(buf2)
	        }
	    }

	    return dishes;
	}
}

export let dishData = new DishData();
