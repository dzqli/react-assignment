import React from 'react';
import {dishData} from './DishData'

class DishRow extends React.Component {
    render(){
        var availableOptions = this.props.dishDict;
        var optionTags = [];
        optionTags.push(<option key={0} value="">---</option>)
        for (var item in availableOptions){
            optionTags.push(<option key={item+1} value={availableOptions[item]}>{availableOptions[item]}</option>);
        }
        return(
            <div className="row">
                <div className="field-label col-md-6 order-md-1">Please Select a Dish</div>
                <div className="field-wrapper col-md-6 order-md-3">                       
                    <select className={"field " + (this.props.hasNameError ? "error" : "")} 
                            id="dish"
                            value={this.props.dish.name}
                            onChange={(e)=>this.props.getDishVal(this.props.id, e)}>
                        {optionTags}
                    </select>
                </div>
                <div className="field-label col-md-6 order-md-2">Please Enter no. of Servings</div>
                <div className="field-wrapper col-md-6 order-md-4">
                    <input className={"small-num-in "  + (this.props.hasNumError ? "error" : "")} 
                           value={this.props.dish.numOrders} 
                           type="number"
                           onChange={(e)=>this.props.getDishCount(this.props.id, e)}/>
                </div>
            </div>
        );
    }
}

//the decision to have validation be immediate followed the fact that the bottom navigation bar is a separate class
class Stage3 extends React.Component {
    constructor(props) {
        super(props);
        this.getDishCount = this.getDishCount.bind(this);
        this.getDishVal = this.getDishVal.bind(this);
        // this.updateDishDict = this.updateDishDict.bind(this)
    };
    getDishCount(index, event) {
        const buf = this.props.dishes;
        buf[index].numOrders = event.target.value;
        this.props.updateModel({
            dishes: buf
        });
    }
    getDishVal(index, event) {
        const buf = this.props.dishes;
        buf[index].name = event.target.value;
        this.props.updateModel({
            dishes: buf
        });
    }
    updateDishDict() {
        var arr = dishData.getDishDict(this.props.restaurant, this.props.meal);
        return arr;
    }
    canAdd() {
        var arr = dishData.getDishDict(this.props.restaurant, this.props.meal);
        return (arr.length > this.props.dishes.length);
    }
    render() {
        var jsxDishRows = [];
        for (var i in this.props.dishes){
            jsxDishRows.push(
                <DishRow
                    getDishCount={this.getDishCount}
                    getDishVal={this.getDishVal}
                    dishDict={this.updateDishDict()}
                    dish={this.props.dishes[i]}
                    hasNumError={!this.props.checkNums()}
                    hasNameError={!this.props.checkNames()}
                    id={i}
                    key={i}
                />
            );
        }
        return(
            <div className={"container-fluid " + (this.props.isHidden ? "swappedOut" : "")}>
                {jsxDishRows}
                <div className="row">
                    <div className="field-wrapper col-sm-1">
                        <button type="button" 
                                className={"btn btn-outline-dark " + (this.canAdd() ? "" : "hidden")}
                                onClick={this.props.addDish}>
                            +
                        </button>
                    </div>
                    <div className="field-wrapper col-sm-1">
                        <button type="button" 
                                className={"btn btn-outline-dark " + (this.props.dishes.length === 1 ? "hidden" : "")}
                                onClick={this.props.deleteDish}>
                            -
                        </button>
                    </div>
                </div>
                <div className="row">
                    <small className={"text-danger centre " + (this.props.checkNums() ? "hidden" : "")}>
                        Please ensure you have the made the required orders for your group. <br/> Maximum {this.props.maxInputs} orders.
                    </small>
                </div>
                <div className="row">
                    <small className={"text-danger centre " + (this.props.checkNames() ? "hidden" : "")}>
                        Please ensure you do not repeat orders or leave them blank.
                    </small>
                </div>
            </div>
        );
    };
};

export default Stage3;
