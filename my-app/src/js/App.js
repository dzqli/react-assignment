import React from 'react';
import '../styles/App.css';
import TopNavBar from './TopNavBar'
import BotNavBar from './BotNavBar'
import FormBody from './FormBody'

class PreOrderApplet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentStage: 0,
            meal: "",
            numPeople: 0,
            restaurant: "",
            dishes: [{name: "", numOrders: 1}],
            maxInputs: 10
        };
        this.changeStage = this.changeStage.bind(this);
        this.updateModel = this.updateModel.bind(this);
        this.checkNums = this.checkNums.bind(this);
        this.checkNames = this.checkNames.bind(this);
        this.submit = this.submit.bind(this);
        this.addDish = this.addDish.bind(this);
        this.deleteDish = this.deleteDish.bind(this);
    };

    submit(){
        console.log({
            meal: this.state.meal,
            numPeople: this.state.numPeople,
            restaurant: this.state.restaurant,
            dishes: this.state.dishes
        })
    }

    addDish(){
        var buf = this.state.dishes;
        buf.push({name: "", numOrders: 1})
        this.setState({
            dishes: buf
        });
    }

    deleteDish(){
        var buf = this.state.dishes;
        buf.pop()
        this.setState({
            dishes: buf
        });
    }

    checkNames(){
        var name = true;
        var nameBuf = [];

        for (var i in this.state.dishes){
            if (nameBuf.includes(this.state.dishes[i].name)){
                name = false;
            }
            if (!this.state.dishes[i].name){
                name = false;
            }
            nameBuf.push(this.state.dishes[i].name);
        }


        return name;
    }

    checkNums(){
        var count = true;
        var total = 0;

        for (var i in this.state.dishes){
            if (this.state.dishes[i].numOrders <= 0 || 
                this.state.dishes[i].numOrders > this.state.maxInputs){

                count = false;
            }
            total = total + parseInt(this.state.dishes[i].numOrders, 10);
        }

        if (total > this.state.maxInputs || total < this.state.numPeople){
            count = false
        }

        return count;
    }

    checkHighestStage() {
        if (!this.state.meal || !(this.state.numPeople > 0 && this.state.numPeople <= this.state.maxInputs)){
            return 0;
        }
        else if (!this.state.restaurant){
            return 1;
        }   
        else if (!(this.checkNames() && this.checkNums())){
            return 2;
        }
        else{
            return 3;
        }
    };

    changeStage(i) {
        this.setState({'currentStage': i});
    };

    updateModel(newModel) {
        this.setState(newModel);
    };

    /*The decision to place the bottom bar (and hence validation) seperate from the form body
      was arbritrary.  If the behaviour ends up not being desired, a simple refactor is simply
      to subsume the bot nav bar as a child of the formbody, bringing the consolidated 
      validation logic into the formbody instead.
    */
    render() {
        return(
            <div className="pre-order-applet container">
                <div className="top-nav-bar container-fluid">
                    <TopNavBar
                        maxStage={this.checkHighestStage()}
                        currentStage={this.state.currentStage}
                        changeStage={this.changeStage}
                    />
                </div>
                <div className="form-body">
                    <FormBody
                        currentStage={this.state.currentStage}
                        updateModel={this.updateModel}
                        maxInputs={this.state.maxInputs}
                        numPeople={this.state.numPeople}
                        meal={this.state.meal}
                        restaurant={this.state.restaurant}
                        dishes={this.state.dishes}
                        checkNums={this.checkNums}
                        checkNames={this.checkNames}
                        addDish={this.addDish}
                        deleteDish={this.deleteDish}
                    />
                </div>
                <div className="bot-nav-bar container-fluid">
                    <BotNavBar
                        maxStage={this.checkHighestStage()}
                        currentStage={this.state.currentStage}
                        changeStage={this.changeStage}
                        submit={this.submit}
                    />            
                </div>
            </div>
        );
    };
};

export default PreOrderApplet;
