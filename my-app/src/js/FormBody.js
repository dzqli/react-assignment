import React from 'react'
import Stage1 from './Stage1'
import Stage2 from './Stage2'
import Stage3 from './Stage3'
import Stage4 from './Stage4'

/*
    The formbody renders all 4 stages (and hides them based on state properties).
    This is done to make encapsulating the form fields in a <form> tag easier, if future 
    devs so choose.
*/
class FormBody extends React.Component {
    render() {
        return(
            <div>
                <Stage1 isHidden={this.props.currentStage !== 0}
                        updateModel={this.props.updateModel}
                        maxInputs={this.props.maxInputs}
                        numPeople={this.props.numPeople}
                        meal={this.props.meal}/>
                <Stage2 isHidden={this.props.currentStage !== 1}
                        updateModel={this.props.updateModel}
                        meal={this.props.meal}
                        restaurant={this.props.restaurant}/>
                <Stage3 isHidden={this.props.currentStage !== 2}
                        restaurant={this.props.restaurant}
                        meal={this.props.meal}
                        updateModel={this.props.updateModel}
                        numPeople={this.props.numPeople}
                        maxInputs={this.props.maxInputs}
                        dishes={this.props.dishes}
                        checkNames={this.props.checkNames}
                        checkNums={this.props.checkNums}
                        addDish={this.props.addDish}
                        deleteDish={this.props.deleteDish}/>
                <Stage4 isHidden={this.props.currentStage !== 3}
                        meal={this.props.meal}
                        numPeople={this.props.numPeople}
                        restaurant={this.props.restaurant}
                        dishes={this.props.dishes}/>
            </div>
        );
    };
};

export default FormBody;
