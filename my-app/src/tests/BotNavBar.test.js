import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import BotNavBar from '../js/BotNavBar';

it('renders without crashing', () => {
  const div = mount(<BotNavBar />);
});

//this suite, much like the top nav bar, is also a problem for extensibility.  

it('allows submit on 4th stage, and next on all others', () => {
  const div = mount(<BotNavBar maxStage={3}
                               currentStage={3}
                               changeStage={()=>{return;}}
                               submit={()=>{return;}}/>);
  expect(div.find('button').at(1).text()).toBe('Submit');
  div.setProps({currentStage: 0});
  div.update();
  expect(div.find('button').at(1).text()).toBe('Next');
});

it('allows previous on all stages except first', () => {
  const div = mount(<BotNavBar maxStage={3}
                               currentStage={0}
                               changeStage={()=>{return;}}
                               submit={()=>{return;}}/>);
  expect(div.find('button').at(0).hasClass('hidden')).toBe(true);
  div.setProps({currentStage: 1});
  div.update();
  expect(div.find('button').at(0).hasClass('hidden')).toBe(false);
});

it('disables next button if maximum allowed stage achieved', () => {
  const div = mount(<BotNavBar maxStage={2}
                               currentStage={2}
                               changeStage={()=>{return;}}
                               submit={()=>{return;}}/>);
  expect(div.find('button[disabled=true]').text()).toBe('Next');
  div.setProps({currentStage: 1});
  div.update();
  expect(div.find('button[disabled=true]')).toHaveLength(0);
});

it('successfully calls state reduction', ()=>{
  const onPrev = sinon.spy();
  const div =  mount(<BotNavBar maxStage={2}
                                currentStage={2}
                                changeStage={onPrev}
                                submit={()=>{return;}}/>);
  div.find('button').at(0).simulate('click');
  expect(onPrev).toHaveProperty('callCount',1);
  expect(onPrev.calledWith(1)).toBe(true);
});

it('successfully calls state increase', ()=>{
  const onNext = sinon.spy();
  const div =  mount(<BotNavBar maxStage={2}
                                currentStage={1}
                                changeStage={onNext}
                                submit={()=>{return;}}/>);
  div.find('button').at(1).simulate('click');
  expect(onNext).toHaveProperty('callCount',1);
  expect(onNext.calledWith(2)).toBe(true);
});

it('successfully calls submit', ()=>{
  const onSubmit = sinon.spy();
  const div =  mount(<BotNavBar maxStage={3}
                                currentStage={3}
                                changeStage={()=>{return;}}
                                submit={onSubmit}/>);
  div.find('button').at(1).simulate('click');
  expect(onSubmit).toHaveProperty('callCount',1);
});