import {dishData} from '../js/DishData';

it('populates dictionary from raw JSON properly', () => {
	expect(dishData.dishes.length).toBeGreaterThan(0);
});


//these test is too specific, but this was glossed over in design of this importer as well. would fix in production.
it('returns restaurant queries by meal', ()=> {
	var meals = ['breakfast','lunch','dinner'];
	for (var i in meals){
		expect(dishData.getRestaurantDict(meals[i]).length).toBeGreaterThan(0);
	}
});

it('returns dish queries by restaurant and meal', ()=> {
	var meal = 'lunch'
	expect(dishData.getDishDict('' , meal).length).toBe(0);
	expect(dishData.getDishDict('Panda Express' , meal).length).toBeGreaterThan(0);
});