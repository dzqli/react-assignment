import React from 'react';
import ReactDOM from 'react-dom';
import Stage4 from '../js/Stage4';
import {shallow} from 'enzyme'

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Stage4 meal={falseModel.meal}
                          numPeople={falseModel.numPeople}
                          restaurant={falseModel.restaurant}
                          dishes={falseModel.dishes}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

//As this page was only designed as a summary page, no futher unit testing is really necessary
/*Only thing is, if this page was any more complex,
  designing this test would reveal how little identification was given to each of the fields,
  which may prove to be a problem for subsequent development*/