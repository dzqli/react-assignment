import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import PreOrderApplet from '../js/App';
import TopNavBar from '../js/TopNavBar';
import BotNavBar from '../js/BotNavBar';
import FormBody from '../js/FormBody';
    
it('renders without crashing', () => {
  const div = mount(<PreOrderApplet />);
  //ReactDOM.unmountComponentAtNode(div);
});

it('renders a top nav bar, a form body, and a bottom nav bar', () => {
  const div = mount(<PreOrderApplet />);
  //ReactDOM.unmountComponentAtNode(div);
  expect(div.find(TopNavBar)).toHaveLength(1);
  expect(div.find(BotNavBar)).toHaveLength(1);
  expect(div.find(FormBody)).toHaveLength(1);
});

it ('provides valid functions to add and remove dishes', ()=>{
    const div = mount(<PreOrderApplet />);
    div.instance().addDish();
    expect(div.state().dishes).toHaveLength(2);
    div.instance().deleteDish();
    expect(div.state().dishes).toHaveLength(1);
});

it('provides a valid function to verify 3rd stage input (dish names)', () => {
    const div = mount(<PreOrderApplet />);

    div.setState(falseModel);
    expect(div.instance().checkNames()).toBe(true);

    div.setState({dishes: [{name: "", numOrders: 1}]});
    expect(div.instance().checkNames()).toBe(false);

    div.setState({dishes: [{name: "Charsu", numOrders: 1}, {name: "Charsu", numOrders: 1}]});
    expect(div.instance().checkNames()).toBe(false);
});

it('provides a valid function to verify 3rd stage input (dish count)', () => {
    const div = mount(<PreOrderApplet />);

    div.setState(falseModel);
    expect(div.instance().checkNums()).toBe(true);

    div.setState({dishes: [{name: "", numOrders: 1}, {name: "", numOrders: falseModel.numPeople-1}]});
    expect(div.instance().checkNums()).toBe(true);

    div.setState({dishes: [{name: "", numOrders: -3}]});
    expect(div.instance().checkNums()).toBe(false);

    div.setState({dishes: [{name: "", numOrders: 1}, {name: "", numOrders: falseModel.numPeople-2}]});
    expect(div.instance().checkNums()).toBe(false);

    div.setState({dishes: [{name: "", numOrders: falseModel.maxInputs+1}]});
    expect(div.instance().checkNums()).toBe(false);
});

it ('provides a valid function to verify stage progress', () => {
    const div = mount(<PreOrderApplet />);

    expect(div.instance().checkHighestStage()).toBe(0);

    div.setState({meal: "dinner"});
    expect(div.instance().checkHighestStage()).toBe(0);

    div.setState({numPeople: 1});
    expect(div.instance().checkHighestStage()).toBe(1);

    div.setState({restaurant: "WcDonalds"});
    expect(div.instance().checkHighestStage()).toBe(2);

    div.setState(falseModel);
    expect(div.instance().checkHighestStage()).toBe(3);
});

it('provides a valid function to update current stage', () => {
    const div = mount(<PreOrderApplet />);
    div.instance().changeStage(3);
    expect(div.state().currentStage).toBe(3);
});


it('provides a valid function to update the model without disturbing other state vars', () => {
    const div = mount(<PreOrderApplet />);
    var name = "hehexd"
    div.setState({restaurant: name});
    div.instance().updateModel({dishes: falseModel.dishes});
    expect(div.state().restaurant).toBe(name);
    expect(div.state().dishes).toBe(falseModel.dishes);
});
