import React from 'react';
import ReactDOM from 'react-dom';
import sinon from 'sinon';
import {shallow, mount} from 'enzyme';
import Stage1 from '../js/Stage1';

it('renders without crashing', () => {
  const component = mount(<Stage1 />);
});

it('shows error on meal selection error, but not when valid', () => {
  const div =  mount(<Stage1 meal=""
                             numPeople={0}
                             updateModel={()=>{return;}}/>);
  expect (div.find('.error')).toHaveLength(0);
  div.find('select').simulate('change', {target: { value: "lunch"}});
  div.setProps({meal: "lunch"})
  div.update();
  expect (div.find('.error')).toHaveLength(0);
  div.find('select').simulate('change', {target: { value: ""}});
  div.setProps({meal: ""})
  div.update();
  expect (div.find('.error')).toHaveLength(1);
});

it('shows error on people number error, but not on valid', () => {
  const div =  mount(<Stage1 meal=""
                             numPeople={0}
                             maxInputs={5}
                             updateModel={()=>{return;}}/>);
  expect (div.find('.error')).toHaveLength(0);
  div.find('input').simulate('change', {target: { value: -1}});
  div.setProps({numPeople: -1})
  div.update();
  expect (div.find('.error')).toHaveLength(1);
  div.find('input').simulate('change', {target: { value: div.props().maxInputs+1}});
  div.setProps({numPeople: div.props().maxInputs+1})
  div.update();
  expect (div.find('.error')).toHaveLength(1);
  div.find('input').simulate('change', {target: { value: div.props().maxInputs-1}});
  div.setProps({numPeople: div.props().maxInputs-1})
  div.update();
  expect (div.find('.error')).toHaveLength(0);
});

it('renders appropriate list items', () => {
  const div =  mount(<Stage1 meal=""
                             numPeople={0}
                             updateModel={()=>{return;}}/>);
  expect (div.find('option')).toHaveLength(4);
});

it('successfully calls update with selected value when meal is selected and resets dishes and restaurant', ()=>{
  const onMealChange = sinon.spy();
  const div =  mount(<Stage1 meal=""
                             numPeople={0}
                             maxInputs={5}
                             updateModel={onMealChange}/>);
  div.find('select').simulate('change', {target: { value: "lunch"}});
  expect(onMealChange).toHaveProperty('callCount',1);
  expect(onMealChange.calledWith(
  {
    meal: "lunch",
    restaurant: "",
    dishes: [{name: "", numOrders: 1}]
  }
  )).toBe(true);
});

it('successfully calls update function with input value when orders is entered', ()=>{
  const onNumPeopleInput = sinon.spy();
  const div =  mount(<Stage1 meal=""
                             numPeople={0}
                             maxInputs={5}
                             updateModel={onNumPeopleInput}/>);
  div.find('input').simulate('change', {target: { value: 4}});
  expect(onNumPeopleInput).toHaveProperty('callCount',1);
  expect(onNumPeopleInput.calledWith(
  {
    numPeople: 4
  }
  )).toBe(true);
});